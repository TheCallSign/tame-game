/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

/**
 *
 * @author stjohn
 */
import java.io.*;
import java.net.*;

public class ServerClient implements Runnable 
{
    BufferedReader inFromUser =
        new BufferedReader(new InputStreamReader(System.in));
    DatagramSocket clientSocket;
        
    InetAddress IPAddress;
    
    int serverPort = 1338;
    
    int clientPort = 1337;
    
    boolean isStopped = false;
    
    Thread t;
    
    @Override
    public void run() {
        
        try{
            this.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public ServerClient(int port) throws Exception {
        clientSocket = new DatagramSocket(port);
        IPAddress = InetAddress.getByName("localhost");
        
        t = new Thread(this, "Server Thread");
        t.start();
        
    }
    
    public void start() throws Exception {
        
        
        
        byte[] receiveData = new byte[1024];
        
        
        // Server loop
        try{
            while(!isStopped){
                System.out.println("Server looping");
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                
                
                
            }
            
        } finally {        
            clientSocket.close();
   
        }
        
        
    }
    
    
        public void sendMessage(int code,String data) throws Exception{
            
            String spacer = "::";
            
            String toSend = "" + code + spacer + data;
            byte[] sendData = new byte[1024];
            System.out.println(toSend);
            sendData = toSend.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 1338);
        
            clientSocket.send(sendPacket);
            
            
        }
        
        public void stop(){
            this.isStopped = true;
        }
           
}