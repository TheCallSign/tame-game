package game;

import java.awt.geom.Rectangle2D;
import static org.lwjgl.opengl.GL11.*;

final class Player{


    private int x = 0;
    private int y = 0;
    private float colorR = 0;
    private float colorG = 0;
    private float colorB = 0;
    
    
    
    private int [] topRight = new int[2];
    public int [] topLeft = new int[2];
    public int [] bottomRight = new int[2];
    private int [] bottomLeft = new int[2];

    private int [] [] currentPolyArea ;
    Rectangle2D.Double collBox;
    
    
    public Player(){
        setX(400);
        setY(300);
        

    }
    
    
    public Player(int x, int y){
        setX(x);
        setY(y);
        

    }


    void draw(){
        glPushMatrix();
        glBegin(GL_QUADS);
                glColor3f(colorR, colorG, colorB);
                glVertex2f(getX() - this.bottomLeft[0], getY() - this.bottomLeft[1]);
                glVertex2f(getX() + this.bottomRight[0], getY() - this.bottomRight[1]);
                glVertex2f(getX() + this.topRight[0], getY() + this.topRight[1]);
                glVertex2f(getX() - this.bottomLeft[0], getY() + this.bottomLeft[1]);
	glEnd();
        glPopMatrix();
       
    }

    void checkScreenBorders(){
        if (this.getX() < 0) this.setX(800);
        if (this.getX() > 800) this.setX(0);
	if (this.getY() < 0) this.setY(600);
	if (this.getY() > 600) this.setY(0);
    }

    int moveUp(int delta){
        return y += 0.35f * delta ;
    }
    int moveDown(int delta){
        return y -= 0.35f * delta ;
    }
    int moveLeft(int delta){
        return x -= 0.35f * delta ;
    }
    int moveRight(int delta){
        return x += 0.35f * delta ;
    }

    public void setBorders(int [] [] playerBox){ // TODO: got to be tr x and tr y (Need x and y cor ords
        this.topRight = playerBox[0];
        this.topLeft = playerBox[1];
        this.bottomRight = playerBox[2];
        this.bottomLeft = playerBox[3];

        currentPolyArea = playerBox;
        
        
    }
    
    /**
     * 
     * @param size int size
     * @return an 2d int array with the box area: (size * 2) ^ 2
     * @see Player.setBorders()
     */
    
    public int [] [] boxSize(int size){
        size = size / 2;
        int [][] box = {
             // X, Y
            {size, size},
            {size, size},
            {size, size},
            {size, size}
        };
        return box;
    }
    
    public boolean  doColl(Rectangle2D.Double other){
        /*
        currentPolyArea = new int [] []{
        {
            getX() - this.bottomLeft[0],
            getY() - this.bottomLeft[1]
        },

        {
            getX() + this.bottomRight[0],
            getY() - this.bottomRight[1]

        },
        {
            getX() + this.topRight[0],
            getY() + this.topRight[1]
        },

        {/
            getX() - this.bottomLeft[0],
            getY() + this.bottomLeft[1]

        }};
        boolean isColled = false;
        int [] thisPoly = new int[64];
        int [] thatPoly = new int[64];
        for(int i = 0; i<otherPoly.length; i++) {
            for(int f = 0; f < otherPoly[i].length; f++ ) {
                // TODO: all the x co-ords minus the y co-ord if they equal 0 then is collieded!
                thatPoly[i] += otherPoly[i][f]; // go through all the variables in that 2d arry and add them up
                
            }
        }
        for(int i = 0; i<currentPolyArea.length; i++) {
            for(int f = 0; f < currentPolyArea[i].length; f++ ) {
                // TODO: all the x co-ords minus the y co-ord if they equal 0 then is collieded!
                thisPoly[i] += currentPolyArea[i][f]; // go through all the variables in that 2d arry and add them up
                
            }
        }
        int out = 0;
        for (int i= 0; i < thisPoly.length; i++){
            out +=  thatPoly[i];
        }
        //System.out.println(out);
         * 
         */
        this.collBox = new Rectangle2D.Double(x,
                y,50, 50);
        boolean isAnyCollision = false;
        int i = 0;
        //while (!isAnyCollision && i < otherPlayers.length) {
           //isAnyCollision = collBox.intersects(otherPlayers[i]);
           isAnyCollision = collBox.intersects(other);
           i++;
        //}
        return isAnyCollision;
        
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @param colorR the colorR to set
     */
    public void setColorR(float colorR) {
        this.colorR = colorR;
    }

    /**
     * @param colorG the colorG to set
     */
    public void setColorG(float colorG) {
        this.colorG = colorG;
    }

    /**
     * @param colorB the colorB to set
     */
    public void setColorB(float colorB) {
        this.colorB = colorB;
    }

    public void setRGB(float colorR, float colorG, float colorB){
        setColorR(colorR);
        setColorG(colorG);
        setColorG(colorB);
    }

}