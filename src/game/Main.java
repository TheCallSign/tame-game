package game;

import java.awt.geom.Rectangle2D;
import java.util.*;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import game.server.*;

public class Main {

	/** position of quad */
	float x = 200, y = 300, x1 = 600, y1 = 300;
	/** angle of quad rotation */
	float rotation = 0;

        // Exit boolean

        boolean isExit = false;
        
	/** time at last frame */
	long lastFrame;

	/** frames per second */
	int fps;
	/** last FPS time */
	long lastFPS;

	/** is VSync Enabled */
	boolean vsync;

        // Make babies (Players)
        // params: starting x and y co-ords
        Player player1 = new Player(600, 300);
        Player player2 = new Player(200, 300);
        
        Server server = new Server();
        
        private int [] [] p1Poly;
        private int [] [] p2Poly;
        
        ServerClient serverclient; 
        
        // This players data map
        HashMap<String, byte[]> playerDataMap = new HashMap<String, byte[]>();
        
        // global player data map
        HashMap<String, Rectangle2D.Double> playerHitBox = new HashMap<String, Rectangle2D.Double>();
        
        // Global player data
        List<HashMap<String, byte[]>> gamePlayersData = 
                new ArrayList<HashMap<String, byte[]>>();
        
        // all possible in-game players
        
        Player [] gamePlayers = new Player[2];
        
        private int lastDir = 0;
        private int lastDir2 = 0;
            
        
        
	public void start() {
               
            // To keep the compatibleity for now:
         
            try {
                Display.setDisplayMode(new DisplayMode(800, 600));
		Display.create();
            } catch (LWJGLException e) {
		e.printStackTrace();
		System.exit(0);
            }

            initGL(); // init OpenGL
            getDelta(); // call once before loop to initialise lastFrame
            lastFPS = getTime(); // call before loop to initialise fps timer
                
               
            //Set up players
            
            gamePlayers[0] = new Player(600, 300);
            gamePlayers[1] = new Player(200, 300);

            gamePlayers[0].setRGB(1.0f, 0, 0);
            gamePlayers[1].setRGB(1.0f, 0, 1.0f);
                
            p1Poly = gamePlayers[0].boxSize(50);
            p2Poly = gamePlayers[1].boxSize(50);
                
            
            //Coll dect stuff
            gamePlayers[0].setBorders(p1Poly);
            gamePlayers[1].setBorders(p2Poly);
            
            playerDataMap.put("CollBox", "".getBytes());
            
            
            gamePlayersData.add(0, playerDataMap);
            
            // Set up players and their IDs
            // using gamePlayers (an associative array)
                
                
                //set up server
                try {
                    serverclient = new ServerClient(1337);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.print("ALMOST FATAL: "+
                            "MULTIPLAYER CLIENTSIDE SERVER UNABLE TO START");
                }
                      
            // Game loop
            while (!Display.isCloseRequested()) {
            	int delta = getDelta();

                update(delta);
		renderGL();

                if (isExit) break;

		Display.update();
                Display.sync(100); // cap fps to 100fps
            }
                serverclient.stop();
            Display.destroy();
        }

	public void update(int delta) {
		
            
            playerHitBox.put("p1",new 
                    Rectangle2D.Double(
                    gamePlayers[0].getX(), gamePlayers[0].getY(), 25,25));
            playerHitBox.put("p2",new 
                    Rectangle2D.Double(gamePlayers[1].getX(), gamePlayers[1].getY(), 50,50));
            


            //Check if player has moved
            
            /* Lots of repetitive code here, 
             * will sort that out in the multiplayer release*/
                    try {
                    serverclient.sendMessage(201, ""+delta);
                    } catch (Exception e ){
                        e.printStackTrace();
                    }
            }
            
            if (Keyboard.isKeyDown(Keyboard.KEY_A)){
                if(!gamePlayers[1].doColl(playerHitBox.get("p1")) && lastDir2 != 1
                        || (gamePlayers[1].doColl(playerHitBox.get("p1"))) ^ lastDir2 == 1 ) {
                    x = gamePlayers[1].moveLeft(delta);
                    lastDir2 = 1;
                } 
                
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                
                if(!gamePlayers[1].doColl(playerHitBox.get("p1"))&& lastDir2 != 2
                        || (gamePlayers[1].doColl(playerHitBox.get("p1"))) ^ lastDir2 == 2) {
                    x = gamePlayers[1].moveRight(delta);
                    lastDir2 = 2;
                } 
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
                
                if(!gamePlayers[1].doColl(playerHitBox.get("p1")) && lastDir2 != 3
                        || (gamePlayers[1].doColl(playerHitBox.get("p1"))) ^ lastDir2 == 3) {
                    y = gamePlayers[1].moveUp(delta);
                    lastDir2 = 3;
                } 
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_S)){
                 if(!gamePlayers[1].doColl(playerHitBox.get("p1")) && lastDir2 != 4
                        || (gamePlayers[1].doColl(playerHitBox.get("p1"))) ^ lastDir2 == 4) {
                    y = gamePlayers[1].moveDown(delta);
                    lastDir2 = 4;
                } 
            }
            boolean isCol = gamePlayers[0].doColl(playerHitBox.get("p2"));
            //////////
            if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
                if(!(isCol && lastDir == 1 )) {
                    x = gamePlayers[0].moveLeft(delta);
                    lastDir = 1;
                } else {
                    
                }
                
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
                
                if(!(gamePlayers[0].doColl(playerHitBox.get("p2")) && lastDir == 2)
                        ||  lastDir != 2 ) {
                    
                    x = gamePlayers[0].moveRight(delta);
                    lastDir = 2;
                } 
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
                
                if(!gamePlayers[0].doColl(playerHitBox.get("p2")) && lastDir != 3
                        || (gamePlayers[0].doColl(playerHitBox.get("p2"))) ^ lastDir == 3 ) {
                    y = gamePlayers[0].moveUp(delta);
                    lastDir = 3;
                } 
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
                 if(!gamePlayers[0].doColl(playerHitBox.get("p2")) && lastDir != 4
                        || (gamePlayers[0].doColl(playerHitBox.get("p2"))) ^ lastDir == 4 ) {
                    y = gamePlayers[0].moveDown(delta);
                    lastDir = 4;
                } 
            }
            

                // Command controls
            if (Keyboard.isKeyDown(Keyboard.KEY_F) && Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)){
                setDisplayMode(800, 600, !Display.isFullscreen());
            }
            while (Keyboard.next()) {
                if (Keyboard.getEventKeyState()) {
                    if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                        isExit = true;
                    }
		}
            }
            
            
            

            gamePlayers[0].checkScreenBorders();
            gamePlayers[1].checkScreenBorders();
            //new int [][] p = {{[0]}};
                
            //gamePlayers[0].doColl(p1Poly);

    
            updateFPS(); // update FPS Counter
        }

	/**
	 * Set the display mode to be used
	 *
	 * @param width The width of the display required
	 * @param height The height of the display required
	 * @param fullscreen True if we want fullscreen mode
	 */
	public void setDisplayMode(int width, int height, boolean fullscreen) {

		// return if requested DisplayMode is already set
                if ((Display.getDisplayMode().getWidth() == width) &&
			(Display.getDisplayMode().getHeight() == height) &&
			(Display.isFullscreen() == fullscreen)) {
			return;
		}

		try {
			DisplayMode targetDisplayMode = null;

			if (fullscreen) {
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				int freq = 0;

				for (int i=0;i<modes.length;i++) {
					DisplayMode current = modes[i];

					if ((current.getWidth() == width) &&
                                                (current.getHeight() == height)) {
						if ((targetDisplayMode == null) 
                                                        || (current.getFrequency()
                                                        >= freq)) {
							if ((targetDisplayMode == null) 
                                                                || (current.getBitsPerPixel()
                                                                > targetDisplayMode.getBitsPerPixel())) {
								targetDisplayMode = current;
								freq = targetDisplayMode.getFrequency();
							}
						}

						// if we've found a match for bpp and frequence against the
						// original display mode then it's probably best to go for this one
						// since it's most likely compatible with the monitor
						if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
						    (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
							targetDisplayMode = current;
							break;
						}
					}
				}
			} else {
				targetDisplayMode = new DisplayMode(width,height);
			}

			if (targetDisplayMode == null) {
				System.out.println("Failed to find value mode: "+width+"x"+height+" fs="+fullscreen);
				return;
			}

			Display.setDisplayMode(targetDisplayMode);
			Display.setFullscreen(fullscreen);

		} catch (LWJGLException e) {
			System.out.println("Unable to setup mode "+width+"x"+height+" fullscreen="+fullscreen + e);
		}
	}

	/**
	 * Calculate how many milliseconds have passed
	 * since last frame.
	 *
	 * @return milliseconds passed since last frame
	 */
	public int getDelta() {
	    long time = getTime();
	    int delta = (int) (time - lastFrame);
	    lastFrame = time;

	    return delta;
	}

	/**
	 * Get the accurate system time
	 *
	 * @return The system time in milliseconds
	 */
	public long getTime() {
	    return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	/**
	 * Calculate the FPS and set it in the title bar
	 */
	public void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("FPS: " + fps);
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}

	public void initGL() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, 800, 0, 600, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}

	public void renderGL() {
		// Clear The Screen And The Depth Buffer
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		// draw quads


                gamePlayers[0].draw();
                gamePlayers[1].draw();

              
	}

	public static void main(String[] argv) {
		Main main = new Main();
		main.start();
	}
}

